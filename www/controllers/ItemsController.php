<?php

namespace app\controllers;

use app\models\Items;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class ItemsController extends Controller {

  public function actionAddItem() {
    Yii::$app->response->format = Response::FORMAT_JSON;

    if(!Yii::$app->request->isAjax)
      return ['success' => false];

    $params = Yii::$app->request->post();
    $success = ( new Items() )->addItem($params);
    $item = Items::findOne(Yii::$app->db->lastInsertID);
    $html = $this->renderAjax('item', ['item' => $item], true);

    return ['success' => $success, 'html' => $html];
  }
  public function actionEditItem() {
    Yii::$app->response->format = Response::FORMAT_JSON;

    if(!Yii::$app->request->isAjax)
      return ['success' => false];

    $params = Yii::$app->request->post();
    $success = Items::findOne($params['id'])->editItem($params);
    $html = $this->renderAjax('item', ['item' => Items::findOne($params['id'])], true);

    return ['success' => $success, 'html' => $html];
  }
  public function actionDeleteItem() {
    Yii::$app->response->format = Response::FORMAT_JSON;

    if(!Yii::$app->request->isAjax)
      return ['success' => false];

    $params = Yii::$app->request->post();
    $success = Items::findOne($params['id'])->delete();

    return ['success' => $success];
  }


  public function actionCompleteItem() {
    Yii::$app->response->format = Response::FORMAT_JSON;

    if(!Yii::$app->request->isAjax)
      return ['success' => false];

    $params = Yii::$app->request->post();;
    $success = Items::findOne($params['id'])->completeItem($params['checked']);
    $html = $this->renderAjax('item', ['item' => Items::findOne($params['id'])], true);

    return ['success' => $success, 'html' => $html];
  }

} 