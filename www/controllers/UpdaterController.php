<?php

namespace app\controllers;

use app\models\Items;
use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;


class UpdaterController extends Controller
{
  public function beforeAction($action) {
    $this->enableCsrfValidation = false;
    return parent::beforeAction($action);
  }

  public function actionRun() {
    Yii::$app->response->format = Response::FORMAT_JSON;
    if(!Yii::$app->request->isAjax)
      return ['success' => false];

    $result = [];
    $query = ( new Items() )->find();
    $countQuery = clone $query;
    $pages = new Pagination([
      'totalCount' => $countQuery->count(),
      'pageSize' => 5
    ]);
    $items = $query
      ->offset($pages->offset)
      ->limit($pages->limit)
      ->orderBy('created_at DESC')
      ->all();
    $html = $this->renderAjax('//items/item-list', [
      'items' => $items
    ]);
    $result['html'] = $html;
    $modals = $this->renderAjax('//_partials/modals', ['items' => $items]);
    $result['modals'] = $modals;

    return $result;
  }

} 