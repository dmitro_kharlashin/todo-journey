<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_151257_add_item_samples extends Migration
{
    public function up()
    {
      $sampleItems = [
        [
          'text' => 'international passport',
          'checked' => 1
        ],
        [
          'text' => 'exit visa',
          'checked' => 0
        ],
        [
          'text' => 'transport ticket',
          'checked' => 0
        ],
        [
          'text' => 'season clothes',
          'checked' => 1
        ],
        [
          'text' => 'medicine chest',
          'checked' => 0
        ],
        [
          'text' => 'photo camera',
          'checked' => 1
        ],
        [
          'text' => 'money',
          'checked' => 1
        ],
      ];

      foreach($sampleItems as $sample){
        $item = ( new \app\models\Items() );
        $item->attributes = $sample;
        $item->save();
      }
    }

    public function down()
    {

    }
}
