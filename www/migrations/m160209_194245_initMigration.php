<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_194245_initMigration extends Migration
{
    public function up()
    {
      $this->createTable('items',
        [
          'id' => Schema::TYPE_PK,
          'text' => Schema::TYPE_STRING . ' NOT NULL ',
          'checked' => Schema::TYPE_BOOLEAN . ' DEFAULT "0"',
          'created_at' => Schema::TYPE_INTEGER,
          'updated_at' => Schema::TYPE_INTEGER
        ]);
    }

    public function down()
    {
      $this->dropTable('items');
    }
}
