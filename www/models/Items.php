<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property integer $id
 * @property string $text
 * @property integer $checked
 * @property integer $created_at
 * @property integer $updated_at
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['checked', 'created_at', 'updated_at'], 'integer'],
            [['text'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'checked' => 'Checked',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

  public function beforeSave($insert) {

    if ($this->isNewRecord){
      $this->created_at = time();
    } else {
      $this->updated_at = time();
    }
    parent::beforeSave($insert);
    return true;
  }

  public function addItem($params) {

    $item = new self();
    $item->text = $params['text'];

    return !$item->save() ? $this->errors : true;
  }
  public function editItem($params) {

    $this->text = $params['text'];

    return !$this->save() ? $this->errors : true;
  }

  public function completeItem($checked) {

    $this->checked = $checked;

    return $this->save();
  }

}
