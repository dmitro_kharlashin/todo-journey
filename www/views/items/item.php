<li class="list-group-item" data-id="<?= $item->id ?>" style="overflow: hidden;">
  <form class="form-horizontal">
    <input type="hidden" name="id" value="<?= $item->id ?>">
    <input type="hidden" name="checked"  value="<?= $item->checked ?>">
    <div class="checkbox visible-lg-inline text-center col-sm-1"
         style="cursor: pointer;padding-top: 0;"
         title="Click to complete item">
      <span class="glyphicon <?= $item->checked ? 'glyphicon-ok-sign text-success' : 'glyphicon-minus-sign text-danger' ?>" style="font-size: 18px;"></span>
    </div>
    <div class="visible-lg-inline col-sm-9">
      <?= $item->checked ? '<s>'.$item->text .'</s>' : $item->text ?>
    </div>
  </form>
  <div class="edit col-sm-1" style="cursor: pointer;"
       title="Click to edit item">
    <span class="glyphicon glyphicon-pencil" data-toggle="modal" data-target="#editItemModal<?= $item->id ?>"></span>
  </div>
  <div class="delete col-sm-1" style="cursor: pointer;"
       title="Click to delete item">
    <span class="glyphicon glyphicon-trash" data-toggle="modal" data-target="#deleteItemModal<?= $item->id ?>"></span>
  </div>
</li>