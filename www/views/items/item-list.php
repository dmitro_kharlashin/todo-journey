<ul id="itemList" class="list-group">
  <?php if(count($items)) foreach ($items as $item) {?>
    <?= $this->render('//items/item', ['item' => $item]) ?>
  <?php }
  else {?>
  <li class="list-group-item empty" style="overflow: hidden;">
    <p class="text-center">Click on 'Add item' button to add first item.</p>
  </li>
  <?php } ?>
</ul>