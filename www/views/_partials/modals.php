<div id="modals" class="modals">
  <?= $this->render('//_partials/add_item_modal', []) ?>
  <div class="edit_modals">
    <?php foreach($items as $item){
      echo $this->render('//_partials/edit_item_modal', ['item' => $item]);
    }?>
  </div>
  <div class="delete_modals">
    <?php foreach($items as $item){
      echo $this->render('//_partials/delete_item_modal', ['item' => $item]);
    }?>
  </div>

</div>