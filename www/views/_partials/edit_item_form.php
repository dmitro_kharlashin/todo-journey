<form id="editItemForm<?= $item->id ?>" class="form-horizontal">
  <input type="hidden" name="id" value="<?= $item->id ?>">
  <div class="form-group">
    <div class="col-sm-12">
      <textarea id="text" name="text" class="form-control" rows="3" placeholder="Enter item text"><?= $item->text ?></textarea>
      <label class="error control-label" for="text"></label>
    </div>
  </div>
</form>