<form id="addItemForm" class="form-horizontal">
  <div class="form-group">
    <div class="col-sm-12">
      <textarea id="text" name="text" class="form-control" rows="3" placeholder="Enter item text"></textarea>
      <label class="error control-label" for="text"></label>
    </div>
  </div>
</form>