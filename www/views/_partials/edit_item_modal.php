<div class="modal fade" id="editItemModal<?= $item->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">Edit item</h4>
      </div>
      <div class="modal-body">
        <?= $this->render('//_partials/edit_item_form', ['item' => $item]) ?>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" onclick="$('#editItemForm<?= $item->id ?>').submit()">Save changes</button>
      </div>
    </div>
  </div>
</div>