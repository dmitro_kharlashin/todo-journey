<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel">What item do you want to add?</h4>
      </div>
      <div class="modal-body">
        <?= $this->render('//_partials/add_item_form', []) ?>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" onclick="$('#addItemForm').submit()">Save item</button>
      </div>
    </div>
  </div>
</div>