<?php

/* @var $this yii\web\View */

$this->title = 'Dmitro Kharlashin. To do journey list';
?>
<div class="site-index">


  <div class="body-content">

    <?= $this->render('//_partials/modals', ['items' => $items]) ?>

    <div class="panel panel-default">
      <!-- Default panel contents -->
      <div class="panel-heading text-center text-capitalize">
        Your trip shopping list
      </div>
      <div class="panel-body">
        <div class="visible-lg-inline-block col-lg-10">
          <p>
            This is shopping list for your journey.
            In this list you can status of item, by click on the left side icon, you can edit and delete items.
            I give you simple examples of shopping items, what can be usefull in your trip.
          </p>
        </div>
        <button type="button" class="btn btn-primary pull-right text-capitalize" data-toggle="modal" data-target="#addItemModal">
          Add item
        </button>
      </div>

      <?= $this->render('//items/item-list', ['items' => $items]) ?>
    </div>

    <?php echo \yii\widgets\LinkPager::widget([
      'pagination' => $pages,
    ]);?>
  </div>
</div>
