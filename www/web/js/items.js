var ITEMS = {

    init: function(){
        this.initAddItemSubmit();
        this.initEditItemSubmit();
        this.initDeleteItemSubmit();
        this.initCheckBoxEvent();
    },

    initAddItemSubmit: function(){
        $('body').on('submit', '#addItemForm', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var form = $(this),
                sendData = form.serializeArray();

            $.ajax({
                url: '?r=items/add-item',
                dataType: 'json',
                type: 'post',
                data: sendData,
                beforeSend: function(){
                    form.find('.error').text('');
                    form.find('.error').closest('.form-group').removeClass('has-error');
                },
                success: function(data){
                    if(data.success == true){
                        $('#addItemModal').modal('hide');
                        $('.modal-backdrop').remove();
                        $('#itemList .empty').remove();
                        $('#itemList').prepend(data.html);
                    } else {
                        var errorText = [];
                        for (i in data.success){
                            errorText = errorText.concat(data.success[i]);
                        }
                        form.find('.error').text(errorText);
                        form.find('.error').closest('.form-group').addClass('has-error');
                    }
                }
            })
        })
    },
    initEditItemSubmit: function(){
        $('body').on('submit', 'form[id^="editItemForm"]', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var $form = $(this),
                id = $form.find('input[name=id]').val(),
                sendData = $form.serializeArray();

            $.ajax({
                url: '?r=items/edit-item',
                dataType: 'json',
                type: 'post',
                data: sendData,
                beforeSend: function(){
                    $form.find('.error').text('');
                    $form.find('.error').closest('.form-group').removeClass('has-error');
                },
                success: function(data){
                    if(data.success == true){
                        $('#editItemModal'+ id).modal('hide');
                        $('.modal-backdrop').remove();
                        $('#itemList [data-id='+ id +']').replaceWith(data.html);
                    } else {
                        var errorText = [];
                        for (i in data.success){
                            errorText = errorText.concat(data.success[i]);
                        }
                        $form.find('.error').text(errorText);
                        $form.find('.error').closest('.form-group').addClass('has-error');
                    }
                }
            })
        })
    },
    initDeleteItemSubmit: function(){
        $('body').on('submit', 'form[id^="deleteItemForm"]', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var $form = $(this),
                id = $form.find('input[name=id]').val(),
                sendData = $form.serializeArray();

            $.ajax({
                url: '?r=items/delete-item',
                dataType: 'json',
                type: 'post',
                data: sendData,
                success: function(data){
                    if(data.success){
                        $('#deleteItemModal'+ id).modal('hide');
                        $('.modal-backdrop').remove();
                        $('#itemList [data-id='+ id +']').remove();
                    }
                }
            })
        })
    },

    initCheckBoxEvent: function(){
        $('body').on('click', '.checkbox', function(e){
            e.preventDefault();
            e.stopImmediatePropagation();

            var $btn = $(this),
                $form = $btn.closest('form'),
                checked = $form.find('[name="checked"]').val();

            $form.find('[name="checked"]').val(checked !== undefined && checked > 0 ? 0 : 1);
            var sendData = $form.serializeArray();

            $.ajax({
                url: '?r=items/complete-item',
                dataType: 'json',
                type: 'post',
                data: sendData,
                success: function(data){
                    if(data.success){
                        $form.closest('.list-group-item').replaceWith(data.html);
                    }
                }
            })
        })
    }

};
ITEMS.init();