var UPDATER = {

    init: function(){
        this.run();
    },

    run: function(){
        var update = function(timeout){
            if(timeout === undefined)
                timeout =15000;

            var per_page = location.search.match(/per-page=\d{1,}/),
                page = location.search.match(/page=\d{1,}/);
            $.ajax({
                url:"?r=updater/run&"+(per_page == null ? 'per-page=5' : per_page )+'&'+(page == null ? 'page=0' : page ),
                type:"POST",
                cache:false,
                timeout:timeout,
                async:true,
                success:function(result){
                    $("#itemList").replaceWith(result.html);
                    if(!$('body').hasClass('modal-open')){
                        $('#modals').replaceWith(result.modals);
                    }
                    setTimeout(function() {
                        update(0, false);
                    },5000);
                }
            });
        };

        update();
    }

};
UPDATER.init();